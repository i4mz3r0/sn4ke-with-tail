let can=document.getElementById("canvas");
let hiscore=Number(localStorage.hiscore)||0;
if(window.innerWidth>600) can.height=can.width=600; 

class Game{
    constructor(){
        this.isOver=false;
        this.isPaused=false;
    }
    pause(){
        if(!this.isPaused){
            c.fillStyle="rgba(0,0,0,0.5)";
            c.fillRect(0,0,can.width,can.height);
            c.fillStyle="#fff";
            c.fillRect(can.width/2-75,can.height/2-125,50,250);
            c.fillRect(can.width/2+50,can.height/2-125,50,250);
            this.isPaused=true;
        }
    }
    overlay(){
        if(this.isOver){
            this.over();
        }
        else if(this.isPaused){
            this.pause();
        }
        else{
            c.fillStyle="#fff";
            c.font="30px monospace";
            c.fillText("score: "+snake.score.toString().padStart(5,"0"),can.width-225,30);
            c.fillText("hi-score: "+hiscore.toString().padStart(5,"0"),can.width-280,70);
        }
    }
    over(){
        this.isOver=true;
        if (snake.score>hiscore){
        	hiscore=snake.score; 
        	localStorage.hiscore=snake.score;
        }
        c.fillStyle="rgba(255,0,0,0.75)";
        c.fillRect(0,0,can.width,can.height);
        c.fillStyle="#fff";
        c.font="150px Arial";
        if(window.innerWidth<600){
            c.fillText("GAME",25,can.height/2-50);
            c.fillText("OVER!",25,can.height/2+50);
        }
        else{
            c.fillText("GAME",75,250);
            c.fillText("OVER!",75,400);
            c.font="50px Arial"
            c.fillText("Your score: "+snake.score,160,450);
        }
    }
}
class Food{
    constructor(){
        this.newpos();
    }
    newpos(){
        this.x=Math.round(Math.random()*(can.width/20-1))*20;  
        this.y=Math.round(Math.random()*(can.height/20-1))*20;  
        // console.log("x: "+this.x+" y: "+this.y);
    }
    draw(){
        c.fillStyle="#f00";
        c.fillRect(this.x,this.y,20,20);
    }
}
class Snake{
    constructor(){
        this.x=can.width/2;
        this.y=can.height/2;
        this.score=0;
        this.velocity=20;
        this.dirX=0;
        this.dirY=0;
        this.tail=[[]];
        this.update();
    }
    changeDir(dir){
        switch(dir){
            case 'w':
            case 'ArrowUp':
            case 'swipeup':
                this.dirX=0;
                this.dirY=-1;
                game.isPaused=false;
                break;
            case 'd':
            case 'ArrowRight':
            case 'swiperight':
                this.dirX=1;
                this.dirY=0;
                game.isPaused=false;
                break;
            case 'a':
            case 'ArrowLeft':
            case 'swipeleft':
                this.dirX=-1;
                this.dirY=0;
                game.isPaused=false;
                break;
            case 's':
            case 'ArrowDown':
            case 'swipedown':
                this.dirX=0;
                this.dirY=1;
                game.isPaused=false;
                break;
            case ' ':
            case 'Enter':
            case 'Escape':
                game.pause();
                break;
        }
    }
    update(){
        c.clearRect(0,0,can.width,can.height);
        this.x=this.x+this.velocity*this.dirX;
        this.y=this.y+this.velocity*this.dirY;
        //borders
        if(this.x<0||this.x>can.width-20||this.y<0||this.y>can.height-20) game.over();

        c.fillStyle="#fff";
        c.fillRect(this.x,this.y,20,20);
        for(let i=0; i<this.tail.length;i++){
            c.fillRect(this.tail[i].x,this.tail[i].y,20,20);
            if(this.x==this.tail[i].x&&this.y==this.tail[i].y) game.over(); 
        }
        if(this.dirX+this.dirY!=0) this.tail.push({x:this.x,y:this.y});
        
        if(this.x==food.x&&this.y==food.y){
        //if(Math.abs(this.x-food.x)<20&&Math.abs(this.y-food.y)<20){
            this.score++;
            food.newpos();
        } 
        else this.tail.shift(); 
    }
}
